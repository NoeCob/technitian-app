import {Injectable, Injector} from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
    HttpClient
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {flatMap, catchError} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {
    accessToken = null;
    apiUrl;
    constructor(private injector: Injector, private storage: Storage, private router: Router) {
        storage.get('authorization').then(data => {
            if (data) {
                this.accessToken = data.access_token;
            }
        });

        this.setApiUrl();
    }

    async setApiUrl() {
        await this.storage.get('api-url').then(data => {
            console.log(data);
            if (data) {
                this.apiUrl = data;
            }
        });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(catchError((errorResponse: HttpErrorResponse) => {
            if (errorResponse.status == 401 && errorResponse.error === 'token_expired') {
                const http = this.injector.get(HttpClient);
                return http.post<any>(`${this.apiUrl}auth/refresh`, {}, {
                    headers: {
                        'Authorization': 'Bearer ' + this.accessToken
                    }
                })
                    .pipe(flatMap(data => {
                        localStorage.setItem('token', data.token);
                        const cloneRequest = request.clone({setHeaders: {'Authorization': `Bearer ${data.token}`}});
                        return next.handle(cloneRequest);
                    }));
            } else {
                if (errorResponse.error === 'token_invalid' || errorResponse.error === 'token_error') {
                    this.router.navigate(['login']);
                }
            }
            return throwError(errorResponse);
        }));

    }
}

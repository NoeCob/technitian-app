import {Injectable} from '@angular/core';
import {Api} from './api';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import {Storage} from '@ionic/storage';

@Injectable()
export class ErrorCodes {

    constructor(private apiService: Api, private storage: Storage) {

    }

    index() {
        const response = new ReplaySubject<any>(1);
        this.apiService.get('error-codes').subscribe(data => {
            response.next(data);
        });
        return response;
    }
}

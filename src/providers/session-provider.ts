import {Injectable} from '@angular/core';
import {Api} from './api';
import {ReplaySubject} from "rxjs";

@Injectable()
export class SessionProvider {

  constructor(public apiService: Api) {
  }

  getToken() {
    const response = new ReplaySubject<any>(1);
    this.apiService.get('auth/token').subscribe(userData => {
      response.next(userData);
    });

    return response;
  }
}

import {Injectable} from '@angular/core';
import {Api} from './api';
import {BehaviorSubject, ReplaySubject} from 'rxjs';
import {Storage} from '@ionic/storage';

@Injectable()
export class Tickets {

    constructor(private apiService: Api, private storage: Storage) {

    }

    index(params) {
        const response = new ReplaySubject<any>(1);
        this.apiService.get('tickets/get_by_technician', params).subscribe(data => {
            response.next(data);
        });
        return response;
    }

    update(params) {
        const response = new ReplaySubject<any>(1);
        this.apiService.put('tickets', params).subscribe(data => {
            response.next(data);
        });
        return response;
    }

    setStatus(params, idTickets) {
        const response = new ReplaySubject<any>(1);
        this.apiService.post('/tickets/' + idTickets + '/setStatus', params).subscribe(data => {
            response.next(data);
        });
        return response;
    }
}

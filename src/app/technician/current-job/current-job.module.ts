import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {CurrentJobPage} from './current-job.page';
import {CloseTicketPage} from '../close-ticket/close-ticket.page';
import {Api, Base64ToBlob, ErrorCodes, Tickets, Loading} from '../../../providers';
import {Camera} from '@ionic-native/camera/ngx';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {Diagnostic} from '@ionic-native/diagnostic/ngx';

const routes: Routes = [
    {
        path: '',
        component: CurrentJobPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [
        CurrentJobPage,
        CloseTicketPage
    ],
    entryComponents: [CloseTicketPage],
    providers: [
        Loading,
        Api,
        ErrorCodes,
        Camera,
        Tickets,
        Geolocation,
        Base64ToBlob,
        Diagnostic
    ]
})
export class CurrentJobPageModule {
}

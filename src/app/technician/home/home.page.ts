import {Component, OnInit} from '@angular/core';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    GoogleMapOptions,
    CameraPosition,
    MarkerOptions,
    Marker,
    Environment
} from '@ionic-native/google-maps';
import {Tickets} from '../../../providers';
import {Dialogs} from '@ionic-native/dialogs/ngx';
import {Loading} from '../../../providers/loading';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
    map: GoogleMap;
    assignations = [];

    constructor(private ticketsService: Tickets, private dialogs: Dialogs, public router: Router,
                private loadingService: Loading, private storage: Storage) {
        /*
        this.loadingService.present().then((d) => {
        });
        this.ticketsService.index().subscribe((data: any) => {
            if (data) {
                this.assignations = data.response.filter(items => items.ticket.status === 'assigned');
            }
            this.loadingService.dismiss();
        });*/
    }

    ngOnInit() {

    }

    loadMap() {

        // This code is necessary for browser
        /*Environment.setEnv({
            'API_KEY_FOR_BROWSER_RELEASE': '(your api key for `https://`)',
            'API_KEY_FOR_BROWSER_DEBUG': '(your api key for `http://`)'
        });*/

        const mapOptions: GoogleMapOptions = {
            camera: {
                target: {
                    lat: 20.974256,
                    lng: -89.618497
                },
                zoom: 10,
                tilt: 30
            }
        };

        /*this.map = GoogleMaps.create('map_canvas', mapOptions);

        const marker: Marker = this.map.addMarkerSync({
            title: 'Ionic',
            icon: 'blue',
            animation: 'DROP',
            position: {
                lat: 20.974256,
                lng: -89.618497
            }
        });
        marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
            alert('clicked');
        });*/
    }

    openTicket(ticket) {
        this.storage.set('assignation', ticket);
        //this.router.navigate(['/technician/current-job']);
        this.router.navigate(['/technician/close-ticket']);
        /*this.dialogs.confirm('¿Esta seguro que desea comenzar el trabajo del ticket?', 'Inicar Ticket', ['Si', 'Cancelar'])
            .then((confirm) => {
                if (confirm === 1) {
                    this.storage.set('assignation', ticket);
                    this.router.navigate(['/technician/current-job']);
                }
            })
            .catch(e => console.log('Error displaying dialog', e));*/
    }

}
